#version 430

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec2 screenPos;

layout(binding = 0) uniform sampler2D tex;
layout(binding = 1) uniform sampler2D g_tex_dataset;

uniform ivec2 g_viewport;
uniform vec4 g_view_minmax;
uniform int pricedif;
uniform float g_clicked_price;

vec2 screenPosToUv(vec2 sp)
{
	return g_view_minmax.xy + (sp * (g_view_minmax.zw - g_view_minmax.xy));
}

void main()
{
	vec2 uv = screenPosToUv(screenPos);
	outColor = texture(tex, uv);
	
	if(pricedif > 0)
	{
		bool ispurple = outColor.x > 0.95 && outColor.y < 0.05 && outColor.z > 0.95;
		float price = texture(g_tex_dataset, uv).x;
		
		if(price > 0 && price < g_clicked_price)
		{
			outColor = vec4((1.0 - price / g_clicked_price) * 0.5, 0.0, 1.0, 1.0);
		}
		if(price > 0 && price > g_clicked_price)
		{
			outColor = vec4(((price - g_clicked_price) / (1.0 - g_clicked_price)) * 0.5, 1.0, 1.0, 1.0);
		}
		
		if(ispurple)
		{
			outColor.b = 0.0;
		}
	}
}
