#version 430

out gl_PerVertex {
    vec4 gl_Position;
};

uniform ivec2 g_viewport;
uniform vec4 g_view_minmax;

layout(binding = 0, std430) restrict readonly buffer bufferData
{
    ivec2 data[];
};

void main() {
	vec2 pos = (vec2(data[gl_VertexID]) + 0.5) / 16384.0;
	pos = (pos - g_view_minmax.xy) / (g_view_minmax.zw - g_view_minmax.xy);
	pos = pos * 2.0 - 1.0;
    gl_Position = vec4(pos, 1.0, 1.0);
}
