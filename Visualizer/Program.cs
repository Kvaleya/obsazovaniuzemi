﻿using System;
using System.IO;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Visualizer
{
	class Program
	{
		const int Size = 16384;
		const int Range = 500;

		struct Point
		{
			public int X, Y;
		}

		static void Main(string[] args)
		{
			string filename = "01.in";

			if(!File.Exists(filename))
			{
				if (args.Length != 1)
				{
					Console.WriteLine("První arg má být soubor s datasetem (pokud není ve stejné složce jako program a nejmenuje se 01.in), na stdin má přijít řešení.");
					return;
				}
				filename = args[0];
				if(!File.Exists(filename))
				{
					Console.WriteLine("Soubor neexistuje.");
					return;
				}
			}

			int count = int.Parse(Console.ReadLine());
			Point[] sol = new Point[count];

			for(int i = 0; i < count; i++)
			{
				var split = Console.ReadLine().Split(' ');
				Point p;
				p.X = int.Parse(split[1]);
				p.Y = int.Parse(split[0]);
				sol[i] = p;
			}

			Console.WriteLine("Loading dataset");
			var data = LoadDataset(filename);
			Console.WriteLine("Generating image");
			ToImage("out.png", data, sol, 32, 32, 1.0);
			Console.WriteLine("Done!");
		}

		static ushort[] LoadDataset(string file)
		{
			ushort[] data = new ushort[Size * Size];
			using(BinaryReader br = new BinaryReader(File.OpenRead(file)))
			{
				for(int i = 0; i < data.Length; i++)
				{
					data[i] = br.ReadUInt16();
				}
			}
			return data;
		}

		static void ToImage(string filename, ushort[] data, Point[] sol, int buySize = 0, double heatMultiplier = 1, double gamma = 2.2)
		{
			// Generate overbuy heatmap
			// Bytes to save mmeory, large overbuy is bad anyway
			byte[] heatmap = new byte[data.Length];

			foreach(var p in sol)
			{
				for(int y = p.Y - Range; y <= p.Y + Range; y++)
				{
					if (y < 0 || y >= Size)
						continue;

					for (int x = p.X - Range; x <= p.X + Range; x++)
					{
						if (x < 0 || x >= Size)
							continue;

						int index = x + y * Size;
						heatmap[index] = (byte)Math.Min(heatmap[index] + 1, 255);
					}
				}
			}

			// Generate colors
			// BGRA
			byte[] pixels = new byte[data.Length * 4];

			for (int i = 0; i < data.Length; i++)
			{
				// Set alpha
				pixels[i * 4 + 3] = 255;

				double localMultiplier = heatMultiplier;
				// Color blue if house
				if (data[i] > 0)
				{
					pixels[i * 4 + 0] = 255;
					pixels[i * 4 + 1] = 128;
					//if(data[i] == 1000)
					//{
					//	// Color cyan if 1000
					//	pixels[i * 4 + 1] = 0xff;
					//}
					localMultiplier *= 2;
				}

				// Color red by heatmap
				pixels[i * 4 + 2] = (byte)Math.Min(Math.Pow(heatmap[i] * localMultiplier / 255.0, 1.0 / gamma) * 255.0, 255);

				// Color test
				//pixels[i * 4 + 0] = 0;
				//pixels[i * 4 + 1] = 0;
				//pixels[i * 4 + 2] = 0;

				//if (data[i] == 1000)
				//{
				//	pixels[i * 4 + 0] = 0xff;
				//	pixels[i * 4 + 1] = 0xff;
				//}
				//if (data[i] > 1000 && data[i] <= 2000)
				//{
				//	pixels[i * 4 + 1] = 0xff;
				//}
				//if (data[i] > 2000 && data[i] <= 3000)
				//{
				//	pixels[i * 4 + 1] = 0xff;
				//	pixels[i * 4 + 2] = 0xff;
				//}
				//if (data[i] > 3000 && data[i] <= 4000)
				//{
				//	pixels[i * 4 + 2] = 0xff;
				//}
				//if (data[i] > 4000 && data[i] <= 5000)
				//{
				//	pixels[i * 4 + 2] = 0xff;
				//	pixels[i * 4 + 0] = 0xff;
				//}
				//if (data[i] > 5000)
				//{
				//	pixels[i * 4 + 0] = 0xff;
				//}
			}

			// Highlight bought houses
			foreach (var p in sol)
			{
				for (int y = p.Y - buySize; y <= p.Y + buySize; y++)
				{
					if (y < 0 || y >= Size)
						continue;

					for (int x = p.X - buySize; x <= p.X + buySize; x++)
					{
						if (x < 0 || x >= Size)
							continue;

						int index = x + y * Size;
						// Green
						pixels[index * 4 + 1] = 255;
					}
				}
			}

			Bitmap bm = new Bitmap(Size, Size);
			var bits = bm.LockBits(new Rectangle(0, 0, Size, Size),
				System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			{
				Marshal.Copy(pixels, 0, bits.Scan0, pixels.Length);
			}
			bm.UnlockBits(bits);
			bm.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
		}
	}
}
