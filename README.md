Veškerý můj kód k úloze https://ksp.mff.cuni.cz/h/ulohy/33/zadani3.html#task-33-3-4

## DataGen

Generátor datasetu. Naparsuje OSM a polygony budov rasterizuje poměrně primiivním algoritmem s artefakty, které se ale ve výsledku málokdy projeví. Data o cenách pozemků jsou množina párů pozice-cena. Toto nejdříve převedu z bodů na plochy tím, že každý bod přidám do fronty, poté pomocí BFS cenu každého bodu propaguju na okolní pixely, dokud nepotkají pixel, co už cenu má. Na výsledek se aplikuje obří gauss blur (radius 256), poté se do něj přidá náhodný šum - každý pixel se pronásobí náhodným číslem mezi 0.8 - 1.2. Ceny převedu do rozsahu 1000-65534, v místech, kde není budova, je vynuluju. Výsledek uložím jako 16bit unsigned inty.

Také umí generovat různé vizualizace dat.

## Solver

Různé pokusy a solvítka na úlohu. Viz vzorové řešení na webu KSP pro popis, co jsem zhruba zkoušel.

## Padder

Tento program dostane jedno řešení a pokusí se vyprodukovat horší řešení s konkrétní cenou. Dobré na trollení účastníků.

## Visualizer

Generuje vizualizaci nějakého řešení.

## Editor

Solver, který používá vysoce sofistikovanou biologickou neuronovou síť (uživatele). Potřebuje OpenGL 4.5 a žere asi 3gb VRAM, je poměrně hodně GPU-driven. Také se dá použít na vizualizaci dat nebo řešení. Shadery jdou editovat za běhu, přepočítání obrazu je ale nutné spustit ručně.
