﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Padder
{
	class Program
	{
		const int Size = 16384;
		const int Range = 500;

		struct Point
		{
			public int X, Y;

			public bool Equals(Point other)
			{
				if (ReferenceEquals(null, other)) return false;
				if (ReferenceEquals(this, other)) return true;
				return X == other.X && Y == other.Y;
			}

			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				if (ReferenceEquals(this, obj)) return true;
				if (obj.GetType() != this.GetType()) return false;
				return Equals((Point)obj);
			}

			public override int GetHashCode()
			{
				unchecked
				{
					return ((int)X * 397) ^ Y;
				}
			}

			public static bool operator ==(Point left, Point right)
			{
				return Equals(left, right);
			}

			public static bool operator !=(Point left, Point right)
			{
				return !Equals(left, right);
			}
		}

		static void Main(string[] args)
		{
			string filename = "01.in";

			if (!File.Exists(filename))
			{
				if (args.Length != 1)
				{
					Console.WriteLine("První arg má být soubor s datasetem (pokud není ve stejné složce jako program a nejmenuje se 01.in), na stdin má přijít na první řádek cílová cena a na zbytek levnější řešení.");
					return;
				}
				filename = args[0];
				if (!File.Exists(filename))
				{
					Console.WriteLine("Soubor neexistuje.");
					return;
				}
			}

			long target = long.Parse(Console.ReadLine());

			int count = int.Parse(Console.ReadLine());
			Point[] sol = new Point[count];

			for (int i = 0; i < count; i++)
			{
				var split = Console.ReadLine().Split(' ');
				Point p;
				p.X = int.Parse(split[1]);
				p.Y = int.Parse(split[0]);
				sol[i] = p;
			}

			Console.WriteLine("Loading dataset...");
			var data = LoadDataset(filename);
			Console.WriteLine($"Padding to {target}...");
			(var padded, var price) = PadSol(sol.ToList(), target, data);
			Console.WriteLine($"Created solution with price {price}, writing to file...");
			PrintSolution(padded, price);
			Console.WriteLine("Done!");
		}

		static ushort[] LoadDataset(string file)
		{
			ushort[] data = new ushort[Size * Size];
			using (BinaryReader br = new BinaryReader(File.OpenRead(file)))
			{
				for (int i = 0; i < data.Length; i++)
				{
					data[i] = br.ReadUInt16();
				}
			}
			return data;
		}

		static void PrintSolution(List<Point> buy, long price)
		{
			using (System.IO.StreamWriter sw = new System.IO.StreamWriter("padded_to_" + price.ToString() + ".out"))
			{
				sw.WriteLine($"{buy.Count}");
				foreach (var b in buy)
				{
					sw.WriteLine($"{b.Y} {b.X}");
				}
			}
		}

		static (List<Point>, long) PadSol(List<Point> original, long target, ushort[] dataset)
		{
			long sumprice = 0;

			HashSet<Point> hashedsol = new HashSet<Point>();
			List<Point> newsol = original.ToList();

			foreach(var p in original)
			{
				sumprice += dataset[p.Y * Size + p.X];
				hashedsol.Add(p);
			}

			Point[] buckets = new Point[65536];

			for(int i = 0; i < buckets.Length; i++)
			{
				buckets[i] = new Point()
				{
					X = -1,
					Y = -1,
				};
			}

			// Fill buckets from lower half of the city
			for(int y = Size / 2; y < Size; y++)
			{
				for (int x = 0; x < Size; x++)
				{
					var p = new Point()
					{
						X = x,
						Y = y,
					};

					int price = dataset[y * Size + x];
					if(price > 0 && !hashedsol.Contains(p))
					{
						buckets[price] = p;
					}
				}
			}

			// Keep adding houses from upper half of city
			bool done = false;
			// Obfuscate the original solution by not starting from 0,0
			const int startX = 378;
			const int startY = 15;
			for (int y = startY; y < Size / 2 && !done; y++)
			{
				for (int x = startX; x < Size && !done; x++)
				{
					var p = new Point()
					{
						X = x,
						Y = y,
					};

					int price = dataset[y * Size + x];
					if (price > 0 && !hashedsol.Contains(p))
					{
						if(sumprice + price > target)
						{
							done = true;
							break;
						}
						newsol.Add(p);
						sumprice += price;
					}
				}
			}
			long diff = target - sumprice;

			int index = (int)diff;

			while(index >= 0)
			{
				var p = buckets[index];
				if (p.X >= 0)
				{
					newsol.Add(p);
					sumprice += dataset[p.Y * Size + p.X];
					break;
				}
				index--;
			}

			return (newsol, sumprice);
		}
	}
}
