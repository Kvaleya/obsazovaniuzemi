﻿using System;
using System.IO;
using OpenTK.Mathematics;
using System.Collections.Generic;

namespace DataGen
{
	class Program
	{
		static void Main(string[] args)
		{
			//RenderBuildings();
			//RenderPrices();
			RenderAll();
		}

		static void RenderBuildings()
		{
			Console.WriteLine("Parsing xml");
			Rasterizer r = new Rasterizer(16384, 16384, new Vector2d(14.2854369, 50.1548411), new Vector2d(14.5999206, 49.9864869));
			//Rasterizer r = new Rasterizer(1024, 1024, new Vector2d(14.4264569, 50.1044822), new Vector2d(14.4344819, 50.1089825));
			var polys = OsmLoader.Load("../../../../../Osmosis/bin/praha_building.osm");
			Console.WriteLine("Rasterizing");
			r.Rasterize(polys);
			Console.WriteLine("Saving");
			r.ToImage("test_cena.png");
		}

		static void RenderPrices()
		{
			Console.WriteLine("Parsing xml");
			Rasterizer r = new Rasterizer(16384, 16384, new Vector2d(14.2854369, 50.1548411), new Vector2d(14.5999206, 49.9864869));
			//Rasterizer r = new Rasterizer(1024, 1024, new Vector2d(14.4264569, 50.1044822), new Vector2d(14.4344819, 50.1089825));
			var prices = OsmLoader.LoadGmlPrices("../../../../../../SED_CenovaMapa_p.gml");
			Console.WriteLine("Rasterizing");
			//r.Rasterize(polys, 0, true);
			r.ExpandPoints(prices);
			r.Blur(7);
			Console.WriteLine("Saving");
			r.ToImage("test_cena_4.png");
		}

		static void RenderAll()
		{
			const double buildingThreshold = 0.5;
			const ushort minPrice = 1000;

			Console.WriteLine("Parsing building xml");
			Rasterizer rbuild = new Rasterizer(16384, 16384, new Vector2d(14.2854369, 50.1548411), new Vector2d(14.5999206, 49.9864869));
			var polys = OsmLoader.Load("../../../../../../Osmosis/bin/praha_building.osm");
			Console.WriteLine("Rasterizing buildings");
			rbuild.Rasterize(polys);
			Console.WriteLine("Saving building image");
			rbuild.ToImage("all_buildings.png", 1);
			Console.WriteLine("Saving building bitmap");
			rbuild.ToBitmap("all_buildings_bits.bin", buildingThreshold);

			Console.WriteLine("Parsing price xml");
			Rasterizer rprice = new Rasterizer(16384, 16384, new Vector2d(14.2854369, 50.1548411), new Vector2d(14.5999206, 49.9864869));
			var prices = OsmLoader.LoadGmlPrices("../../../../../../SED_CenovaMapa_p.gml");
			Console.WriteLine("Rasterizing prices");
			rprice.ExpandPoints(prices, 65535.0 / 71000.0);
			rprice.Blur(7);
			Console.WriteLine("Saving price heatmap");
			rprice.ToImage("all_price_heatmap.png", 65535);

			Console.WriteLine("Multiplying");
			rprice.Multiply(rbuild);

			Console.WriteLine("Saving price ushort");
			rprice.ToUshortmap("all_combined_ushort.bin", rbuild, minPrice, buildingThreshold);

			Console.WriteLine("Saving price image");
			rprice.ToImage("all_combined.png", 65535, true);
		}
	}
}
