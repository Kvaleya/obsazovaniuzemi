﻿using System;
using System.Collections.Generic;
using OpenTK.Mathematics;

namespace DataGen
{
	struct PriceSample
	{
		public Vector2d Position;
		public uint Price;
	}
}
