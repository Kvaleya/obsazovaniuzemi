﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK.Mathematics;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;

namespace DataGen
{
	class Rasterizer
	{
		int _w { get { return _size.X; } }
		int _h { get { return _size.Y; } }
		Vector2i _size;

		double[] _data;
		double[] _dataSwap;
		Vector2d _aabbMin, _aabbMax;

		public double this[int x, int y]
		{
			get
			{
				if (x < 0 || x >= _w)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= _h)
					throw new ArgumentOutOfRangeException(nameof(y));
				return _data[x + y * _w];
			}
			set
			{
				if (x < 0 || x >= _w)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= _h)
					throw new ArgumentOutOfRangeException(nameof(y));
				_data[x + y * _w] = value;
			}
		}

		public Rasterizer(int width, int height, Vector2d min, Vector2d max)
		{
			_size = new Vector2i(width, height);
			_aabbMin = new Vector2d(Math.Min(min.X, max.X), Math.Min(min.Y, max.Y));
			_aabbMax = new Vector2d(Math.Max(min.X, max.X), Math.Max(min.Y, max.Y));
			_data = new double[_w * _h];
			_dataSwap = new double[_w * _h];
		}

		void Clear(double value)
		{
			for (int i = 0; i < _data.Length; i++)
			{
				_data[i] = value;
				_dataSwap[i] = value;
			}
		}

		Vector2i PosToPixel(Vector2d p)
		{
			Vector2i result = new Vector2i(0);

			for(int i = 0; i < 2; i++)
			{
				result[i] = (int)Math.Floor((p[i] - _aabbMin[i]) / (_aabbMax[i] - _aabbMin[i]) * _size[i]);
				if (result[i] < 0)
					result[i] = 0;
				if (result[i] >= _size[i])
					result[i] = _size[i];
			}

			return result;
		}

		Vector2d PixelToPos(Vector2i pixel)
		{
			Vector2d p = Vector2d.Zero;
			for(int i = 0; i < 2; i++)
			{
				p[i] = _aabbMin[i] + (pixel[i] + 0.5) / _size[i] * (_aabbMax[i] - _aabbMin[i]);
			}
			return p;
		}

		public void Rasterize(List<Polygon> polys, double value = 1, bool usePolyValue = false)
		{
			int its = 0;
			Parallel.ForEach(polys, poly =>
			{
				if (its % 200 == 0)
				{
					Console.WriteLine($"{its} / {polys.Count}");
				}

				double localValue = value;

				if(usePolyValue)
				{
					localValue = (double)poly.Price;
				}

				Interlocked.Increment(ref its);

				Vector2d min, max;
				(min, max) = poly.GetAabb();

				Vector2i imin = PosToPixel(min) - Vector2i.One;
				Vector2i imax = PosToPixel(max) + Vector2i.One;

				imin.X = Math.Max(0, imin.X);
				imin.Y = Math.Max(0, imin.Y);

				imax.X = Math.Min(_w - 1, imax.X);
				imax.Y = Math.Min(_h - 1, imax.Y);

				for (int y = imin.Y; y <= imax.Y; y++)
				{
					for (int x = imin.X; x <= imax.X; x++)
					{
						Vector2d pos = PixelToPos(new Vector2i(x, y));
						if (poly.PointInPolygon(pos))
						{
							// Rasterize things upside down, because we are in the north hemisphere
							this[x, _h - y - 1] = localValue;
						}
					}
				}
			});
			//foreach(var poly in polys)
			//{
				
			//}
		}

		public void ToBitmap(string filename, double threshold = 0.5)
		{
			using(BinaryWriter bw = new BinaryWriter(new FileStream(filename, FileMode.Create)))
			{
				int bitIndex = 0;
				byte bits = 0;
				for(int i = 0; i < _data.Length; i++)
				{
					if (_data[i] >= threshold)
						bits = (byte)(bits | (1 << bitIndex));

					bitIndex++;
					if(bitIndex == 8)
					{
						bw.Write(bits);
						bitIndex = 0;
						bits = 0;
					}
				}
			}
		}

		public void ToUshortmap(string filename, Rasterizer buildings, ushort minPrice = 1, double threshold = 0.5)
		{
			using (BinaryWriter bw = new BinaryWriter(new FileStream(filename, FileMode.Create)))
			{
				for (int i = 0; i < _data.Length; i++)
				{
					ushort v = 0;
					if(buildings._data[i] >= threshold)
					{
						//v = (ushort)Math.Max((int)v, minPrice);
						double p = _data[i];
						p = minPrice + (p / 65535.0) * (65535.0 - minPrice);
						v = (ushort)Math.Min(Math.Max(p, minPrice), 0xfffe);
					}
					bw.Write(v);
				}
			}
		}

		public void ToImage(string filename, double max = 71000, bool forceBlue = false)
		{
			byte[] pixels = new byte[_data.Length * 4];

			for(int i = 0; i < _data.Length; i++)
			{
				uint color = 0xff000000;
				color |= (uint)(Math.Min((int)(_data[i] / max * 255), 255) << 8);
				if (_data[i] > 0 && forceBlue)
					color |= 0x80;

				for(int j = 0; j < 4; j++)
				{
					pixels[i * 4 + j] = (byte)((color >> (j * 8)) & 0xff);
				}
			}

			Bitmap bm = new Bitmap(_w, _h);
			var bits = bm.LockBits(new Rectangle(0, 0, _w, _h),
				System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			{
				Marshal.Copy(pixels, 0, bits.Scan0, pixels.Length);
			}
			bm.UnlockBits(bits);
			bm.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
		}

		public void ExpandPoints(List<PriceSample> initial, double priceMultiply = 1)
		{
			Clear(-1);

			Vector2i pack(Vector2i pixel, int price)
			{
				return new Vector2i((pixel.X & 0xffff) | ((pixel.Y & 0xffff) << 16), price);
			}

			(Vector2i, int) unpack(Vector2i packed)
			{
				Vector2i un = new Vector2i(packed.X & 0xffff, (packed.X >> 16) & 0xffff);
				return (un, packed.Y);
			}

			Queue<Vector2i> queue = new Queue<Vector2i>();

			foreach(var i in initial)
			{
				var pos = PosToPixel(i.Position);

				if (pos.X < 0 || pos.Y < 0 || pos.X >= _w || pos.Y >= _h)
					continue;

				if (this[pos.X, pos.Y] >= 0)
					continue;

				int p = Math.Min((int)((i.Price & 0x00ffffff) * priceMultiply), 0xffff);

				this[pos.X, pos.Y] = p;

				queue.Enqueue(pack(pos, p));
			}

			int its = 0;

			while(queue.Count > 0)
			{
				(Vector2i p, int price) = unpack(queue.Dequeue());

				if (its % 1000000 == 0)
				{
					Console.WriteLine($"{its} / {16384 * 16384}");
				}

				its++;

				for(int dy = -1; dy <= 1; dy++)
				{
					for (int dx = -1; dx <= 1; dx++)
					{
						if (Math.Abs(dx) == 1 && Math.Abs(dy) == 1)
							continue;

						Vector2i neighbour = new Vector2i(p.X + dx, p.Y + dy);
						if(neighbour.X < 0 || neighbour.Y < 0
							|| neighbour.X >= _w || neighbour.Y >= _h
							|| this[neighbour.X, neighbour.Y] >= 0)
						{
							continue;
						}

						this[neighbour.X, neighbour.Y] = price;
						queue.Enqueue(pack(neighbour, price));
					}
				}
			}
		}

		public void Blur(int epochs)
		{
			for(int i = 0; i < epochs; i++)
			{
				Console.WriteLine($"Blur epoch {i+1} / {epochs}");
				BlurStage(1 << i);
			}
		}

		// Also introduces noise
		void BlurStage(int stride)
		{
			double sample(double[] d, int x, int y)
			{
				if (x < 0)
					x = 0;
				if (y < 0)
					y = 0;
				if (x >= _w)
					x = _w - 1;
				if (y >= _h)
					y = _h - 1;
				double val = d[x + y * _w];
				if (val < 0)
					return 0;
				return val;
			}

			var tmp = _data;
			_data = _dataSwap;
			_dataSwap = tmp;

			int[,] kernel = new int[,]
			{
				{1, 2, 1},
				{2, 4, 2},
				{1, 2, 1},
			};
			int kernel_sum = 16;

			Parallel.For(0, _h, y =>
			{
				Random r = new Random(y * stride);

				for(int x = 0; x < _w; x++)
				{
					double sum = 0.0;
					
					for (int ky = -1; ky <= 1; ky++)
					{
						for (int kx = -1; kx <= 1; kx++)
						{
							sum += sample(_dataSwap, x + kx * stride, y + ky * stride) * kernel[kx + 1, ky + 1];
						}
					}

					sum /= kernel_sum;

					double factor = (r.NextDouble() * 2.0 - 1.0) * 0.2 + 1.0;

					this[x, y] = sum * factor;
				}
			});
		}

		public void Multiply(Rasterizer other)
		{
			for(int i = 0; i < _data.Length; i++)
			{
				_data[i] = _data[i] * other._data[i];
			}
		}
	}
}
