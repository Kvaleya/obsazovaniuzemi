﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK.Mathematics;

namespace DataGen
{
	class Polygon
	{
		List<Vector2d> _points;
		public uint Price;

		public Polygon(IEnumerable<Vector2d> points, uint price = 0)
		{
			_points = points.ToList();
			Price = price;
		}

		public (Vector2d, Vector2d) GetAabb()
		{
			Vector2d min = new Vector2d(double.PositiveInfinity), max = new Vector2d(double.NegativeInfinity);

			foreach (var p in _points)
			{
				for (int i = 0; i < 2; i++)
				{
					min[i] = Math.Min(min[i], p[i]);
					max[i] = Math.Max(max[i], p[i]);
				}
			}

			return (min, max);
		}

		// https://stackoverflow.com/questions/4543506/algorithm-for-intersection-of-2-lines
		static bool LineToSegmentIntersection(Vector2d line0, Vector2d line1, Vector2d seg0, Vector2d seg1)
		{
			Vector2d lineDir = (line1 - line0).Normalized();
			Vector2d segDir = (seg1 - seg0).Normalized();

			double A1 = lineDir.Y;
			double B1 = -lineDir.X;
			double C1 = Vector2d.Dot(line0, new Vector2d(A1, B1));

			double A2 = segDir.Y;
			double B2 = -segDir.X;
			double C2 = Vector2d.Dot(seg0, new Vector2d(A2, B2));

			double delta = A1 * B2 - A2 * B1;

			if (delta == 0)
				return false; // parallel

			double x = (B2 * C1 - B1 * C2) / delta;
			double y = (A1 * C2 - A2 * C1) / delta;

			double distSeg = Vector2d.Dot(new Vector2d(x, y) - seg0, segDir);
			if (distSeg < 0)
				return false;
			if (distSeg >= (seg0 - seg1).Length)
				return false;
			double distLine = Vector2d.Dot(new Vector2d(x, y) - line0, lineDir);
			if (distLine < 0)
				return false;
			return true;
		}

		// Return True if the point is in the polygon.
		public bool PointInPolygon(Vector2d query)
		{
			int intersections = 0;

			for (int i = 0; i < _points.Count; i++)
			{
				Vector2d p0 = _points[i];
				Vector2d p1 = _points[(i + 1) % _points.Count];
				if (LineToSegmentIntersection(query, query + Vector2d.UnitX + new Vector2d(0.001, 0.0001), p0, p1))
					intersections++;
			}

			// Needs to be flipped I don't know why :(
			if (!((intersections % 2) == 1))
				return true;

			// TODO: check if pixel intersects lines or points for conservative rasterization

			return false;
		}
	}
}
