﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Globalization;
using OpenTK.Mathematics;

namespace DataGen
{
	class OsmLoader
	{
		public static List<Polygon> Load(string file)
		{
			Dictionary<long, Vector2d> points = new Dictionary<long, Vector2d>();
			List<List<long>> ways = new List<List<long>>();

			XmlDocument xmlDoc = new XmlDocument(); // Create an XML document object
			xmlDoc.Load(file); // Load the XML document from the specified file

			foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
			{
				if(node.Name == "node")
				{
					long id = 0;
					Vector2d pos = Vector2d.Zero;
					foreach(XmlAttribute attribute in node.Attributes)
					{
						if(attribute.Name == "id")
						{
							id = long.Parse(attribute.InnerText);
						}
						if (attribute.Name == "lat")
						{
							pos.Y = double.Parse(attribute.InnerText, System.Globalization.CultureInfo.InvariantCulture);
						}
						if (attribute.Name == "lon")
						{
							pos.X = double.Parse(attribute.InnerText, System.Globalization.CultureInfo.InvariantCulture);
						}
					}
					points.Add(id, pos);
				}
				if(node.Name == "way")
				{
					List<long> way = new List<long>();
					foreach (XmlNode child in node.ChildNodes)
					{
						if (child.Name == "nd")
						{
							long id = 0;
							foreach (XmlAttribute attribute in child.Attributes)
							{
								if (attribute.Name == "ref")
								{
									id = long.Parse(attribute.InnerText);
								}
							}
							way.Add(id);
						}
					}
					ways.Add(way);
				}
			}

			List<Polygon> polys = new List<Polygon>();

			foreach (var w in ways)
			{
				List<Vector2d> l = new List<Vector2d>();
				foreach (long i in w)
				{
					if(points.ContainsKey(i))
						l.Add(points[i]);
				}
				polys.Add(new Polygon(l));
			}

			return polys;
		}

		static IEnumerable<XmlNode> FindNodesRecursive(XmlNode root, string name)
		{
			foreach(XmlNode child in root.ChildNodes)
			{
				if(child.Name == name)
				{
					yield return child;
				}
				else
				{
					foreach(var c2 in FindNodesRecursive(child, name))
					{
						yield return c2;
					}
				}
			}
		}

		public static List<PriceSample> LoadGmlPrices(string file)
		{
			Random random = new Random(41);

			List<Polygon> polys = new List<Polygon>();

			List<PriceSample> prices = new List<PriceSample>();

			XmlDocument xmlDoc = new XmlDocument(); // Create an XML document object
			xmlDoc.Load(file); // Load the XML document from the specified file

			uint priceMin = uint.MaxValue;
			uint priceMax = 0;

			foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
			{
				foreach (XmlNode n2 in node.ChildNodes)
				{
					uint price = 0xff000000;
					List<Vector2d> points = new List<Vector2d>();
					if (n2.Name == "fme:SED_CenovaMapa_p")
					{
						foreach (XmlNode n3 in n2.ChildNodes)
						{
							if(n3.Name == "fme:CENA")
							{
								if(!uint.TryParse(n3.InnerText, out price))
								{
									price = 0xff000000;
									//price = (uint)random.Next(0, 0x01000000) | 0xff000000;
								}
								else
								{
									priceMin = Math.Min(priceMin, price);
									priceMax = Math.Max(priceMax, price);
									price |= 0xff000000;
								}
								//price = (uint)random.Next(0, 0x01000000) | 0xff000000;
							}

							if(price < 1)
							{
								continue;
							}

							foreach(var poslist in FindNodesRecursive(n3, "gml:posList"))
							{
								//List<Vector2d> points = new List<Vector2d>();

								var numbers = poslist.InnerText.Split(' ');

								Vector2d point = Vector2d.Zero;

								int count = 0;

								for(int i = 0; i < numbers.Length; i++)
								{
									double d = 0;
									if (!double.TryParse(numbers[i], NumberStyles.Any, CultureInfo.InvariantCulture, out d))
										break;
									if(d == 0)
									{
										count = 0;
										continue;
									}

									if(count == 0)
									{
										point.Y = d;
									}
									else
									{
										point.X = d;
										prices.Add(new PriceSample()
										{
											Position = point,
											Price = price,
										});
										//points.Add(point);
										//polys.Add(new Polygon(new List<Vector2d>()
										//{
										//	point,
										//	point + new Vector2d(0.0002, 0),
										//	point + new Vector2d(0.0, 0.0002),
										//}, price));
									}
									count++;
								}
							}
						}

						// emit polygon
						//polys.Add(new Polygon(points, price));
					}
				}
			}

			Console.WriteLine($"Price min: {priceMin} max: {priceMax}");

			//return polys;
			return prices;
		}
	}
}
