﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Solver
{
	class PricePerfSolver : ISolver
	{
		public string Name { get { return "priceperf"; } }

		public int MaxIters;

		public PricePerfSolver(int maxIters = 3)
		{
			MaxIters = maxIters;
		}

		public List<Vector2i> Solve(PriceMap prices, Bitmap buildings)
		{
			long getPrice(int x, int y)
			{
				if (x < 0 || y < 0 || x >= Constants.Size || y >= Constants.Size)
					return 0;
				if (buildings[x, y])
					return prices[x, y];
				return 0;
			}

			var buy = new List<Vector2i>();

			Object sync = new object();

			PrefixSum sum = new PrefixSum(prices, buildings);

			long pricesum = 0;

			long buildingCount = buildings.TotalSet;

			while (true)
			{
				Console.WriteLine($"PP iter {buy.Count+1}");
				bool finished = true;

				double bestPricePerf = double.NegativeInfinity;
				Vector2i bestSpot = Vector2i.Zero;

				System.Threading.Tasks.Parallel.For(0, Constants.Size, y =>
				{
					double localBest = double.NegativeInfinity;
					Vector2i localSpot = Vector2i.Zero;
					bool localFinished = false;
					for (int x = 0; x < Constants.Size; x++)
					{
						if (buildings[x, y])
						{
							localFinished = false;

							// Evaluate
							double pricePerf = sum.Sum(
								new Vector2i(x - Constants.Range, y - Constants.Range),
								new Vector2i(x + Constants.Range, y + Constants.Range))
								/ (double)getPrice(x, y);

							if (pricePerf > localBest)
							{
								localBest = pricePerf;
								localSpot = new Vector2i(x, y);
							}
						}
					}

					lock(sync)
					{
						finished = finished && localFinished;
						if (localBest > bestPricePerf)
						{
							bestPricePerf = localBest;
							bestSpot = localSpot;
						}
					}
				});
				//for (int y = 0; y < Constants.Size; y++)
				//{
				//	for (int x = 0; x < Constants.Size; x++)
				//	{
				//		if(buildings[x, y])
				//		{
				//			finished = false;

				//			// Evaluate
				//			double pricePerf = sum.Sum(
				//				new Vector2i(x - Constants.Range, y - Constants.Range),
				//				new Vector2i(x + Constants.Range, y + Constants.Range))
				//				/ (double)getPrice(x, y);

				//			if (pricePerf > bestPricePerf)
				//			{
				//				bestPricePerf = pricePerf;
				//				bestSpot = new Vector2i(x, y);
				//			}
				//		}
				//	}
				//}

				if(finished)
				{
					break;
				}
				else
				{
					buy.Add(bestSpot);
					buildings.SetSquare(bestSpot.X, bestSpot.Y, false);
					pricesum += prices[bestSpot.X, bestSpot.Y];
				}

				if(buy.Count >= MaxIters)
				{
					break;
				}

				int startX = bestSpot.X - Constants.Range - 1;
				int startY = bestSpot.Y - Constants.Range - 1;

				if (startX < 0)
					startX = 0;
				if (startY < 0)
					startY = 0;

				Console.WriteLine($"Fixing sum, price: {pricesum}; so far covered: {(buildingCount - buildings.TotalSet) / (double)buildingCount}");
				sum.Compute(prices, buildings, startX, startY);
			}

			// Finish using hungry
			HungrySolver.SolveHungry(buy, prices, buildings);

			return buy;
		}
	}
}
