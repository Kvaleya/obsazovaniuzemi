﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK.Mathematics;

namespace Solver
{
	interface ISolver
	{
		string Name { get; }
		List<Vector2i> Solve(PriceMap prices, Bitmap buildings);
	}

	class Program
	{
		public static List<Vector2i> LoadSol(string file)
		{
			List<Vector2i> sol = new List<Vector2i>();
			using (StreamReader sr = new StreamReader(file))
			{
				int count = int.Parse(sr.ReadLine());
				for (int i = 0; i < count; i++)
				{
					int x, y;
					var line = sr.ReadLine().Split(' ');
					x = int.Parse(line[1]);
					y = int.Parse(line[0]);
					sol.Add(new Vector2i(x, y));
				}
			}
			return sol;
		}

		public static List<List<Vector2i>> LoadSolutions(string dir)
		{
			var files = Directory.GetFiles(dir, "*.out");
			List<List<Vector2i>> sols = new List<List<Vector2i>>();

			foreach(var f in files)
			{
				try
				{
					List<Vector2i> sol = new List<Vector2i>();
					using (StreamReader sr = new StreamReader(f))
					{
						int count = int.Parse(sr.ReadLine());
						for(int i = 0; i < count; i++)
						{
							int x, y;
							var line = sr.ReadLine().Split(' ');
							x = int.Parse(line[1]);
							y = int.Parse(line[0]);
							sol.Add(new Vector2i(x, y));
						}
					}
					sols.Add(sol);
				}
				catch(Exception e)
				{

				}
			}
			return sols;
		}

		public static long GetPrice(List<Vector2i> buy, PriceMap prices)
		{
			long sum = 0;
			foreach(var point in buy)
			{
				sum += (long)prices[point.X, point.Y];
			}
			return sum;
		}

		public static bool Validate(List<Vector2i> buy, PriceMap prices, Bitmap buildings)
		{
			foreach(var p in buy)
			{
				buildings.SetSquare(p.X, p.Y, false);
			}

			for(int y = 0; y < Constants.Size; y++)
			{
				for (int x = 0; x < Constants.Size; x++)
				{
					if (buildings[x, y])
						return false;
				}
			}

			return true;
		}

		public static void SaveSolution(string name, List<Vector2i> buy, long price)
		{
			using(System.IO.StreamWriter sw = new System.IO.StreamWriter(name + "_" + price.ToString() + ".out"))
			{
				sw.WriteLine($"{buy.Count}");
				foreach(var b in buy)
				{
					sw.WriteLine($"{b.Y} {b.X}");
				}
			}
		}

		static void EvalSolver(ISolver solver, PriceMap prices, Bitmap buildings)
		{
			Console.WriteLine($"Running solver {solver}");
			var solution = solver.Solve(prices, buildings.Duplicate());

			var sum = GetPrice(solution, prices);
			bool valid = Validate(solution, prices, buildings.Duplicate());

			if(valid)
			{
				SaveSolution(solver.Name, solution, sum);
			}

			Console.WriteLine($"Valid: {valid} Total price: {sum} Bought buildings: {solution.Count}");
		}

		static void Main(string[] args)
		{
			Console.WriteLine("Loading dataset");
			(var prices, var buildings) = PriceMap.FromFile("../../../../01.in");

			RandomSolver.SolveRolling(prices, buildings, new ProximityMap(buildings), 3000);

			//EvalSolver(new SimpleSolver(), prices, buildings);
			//EvalSolver(new PricePerfSolver(), prices, buildings);
			//EvalSolver(new HungrySolver(), prices, buildings);

			EvalSolver(new RandomSolver(), prices, buildings);
			//EvalSolver(new HungryPricePerfSolver(), prices, buildings);

			return;

			Console.WriteLine("Awaiting queries in format \"X Y\"");
			while (true)
			{
				var spl = Console.ReadLine().Split(' ');
				int x, y;
				if(spl.Length != 2 || !int.TryParse(spl[0], out x) || !int.TryParse(spl[1], out y))
				{
					Console.WriteLine("Bad format!");
					continue;
				}
				Console.WriteLine($"Price: {prices[x, y]} Building: {buildings[x, y]}");
			}
		}
	}
}
