﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Solver
{
	class HungrySolver : ISolver
	{
		public string Name { get { return "hungry"; } }

		public static Vector2i FindBest(PriceMap prices, Bitmap buildings, int cx, int cy)
		{
			Vector2i best = new Vector2i(cx, cy);
			int bestPrice = int.MaxValue;
			for (int y = cy - Constants.Range; y <= cy + Constants.Range; y++)
			{
				if (y < 0 || y >= prices.Height)
				{
					continue;
				}
				for (int x = cx - Constants.Range; x <= cx + Constants.Range; x++)
				{
					if (x < 0 || x >= prices.Width)
					{
						continue;
					}
					
					if(prices.ExistsBuilding(x, y) && buildings[x, y] && prices[x, y] < bestPrice)
					{
						bestPrice = prices[x, y];
						best = new Vector2i(x, y);
					}
				}
			}
			return best;
		}

		public List<Vector2i> Solve(PriceMap prices, Bitmap buildings)
		{
			List<Vector2i> buy = new List<Vector2i>();

			SolveHungry(buy, prices, buildings);

			return buy;
		}

		public static void SolveHungry(List<Vector2i> buy, PriceMap prices, Bitmap buildings)
		{
			for (int y = 0; y < prices.Height; y++)
			{
				for (int x = 0; x < prices.Width; x++)
				{
					if (buildings[x, y])
					{
						var best = FindBest(prices, buildings, x, y);
						buy.Add(best);
						buildings.SetSquare(best.X, best.Y, false);
					}
				}
			}
		}
	}
}
