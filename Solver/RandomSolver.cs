﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace Solver
{
	class RandomSolver : ISolver
	{
		public string Name { get { return "random"; } }

		static Vector2i FindBest(PriceMap prices, int cx, int cy, int range = Constants.Range)
		{
			Vector2i best = new Vector2i(cx, cy);
			int bestPrice = int.MaxValue;
			for (int y = cy - range; y <= cy + range; y++)
			{
				if (y < 0 || y >= prices.Height)
				{
					continue;
				}
				for (int x = cx - range; x <= cx + range; x++)
				{
					if (x < 0 || x >= prices.Width)
					{
						continue;
					}

					if (prices.ExistsBuilding(x, y) && prices[x, y] < bestPrice)
					{
						bestPrice = prices[x, y];
						best = new Vector2i(x, y);
					}
				}
			}
			return best;
		}

		public List<Vector2i> Solve(PriceMap prices, Bitmap buildings)
		{
			const int startSeed = 2222;
			const int tries = 16;
			const int hits = 4000;
			const int findingRange = 40;

			ProximityMap proxy = new ProximityMap(buildings);

			object sync = new object();

			List<Vector2i> bestSol = new List<Vector2i>();
			long bestPrice = long.MaxValue;

			Console.WriteLine("Running random solver");

			int completed = 0;

			//for(int t = 0; t < tries; t++)
			Parallel.For(0, tries, t =>
			{
				var sol = SolveRandom(prices, buildings.Duplicate(), proxy, startSeed + t * 31, hits, findingRange);
				long p = Program.GetPrice(sol, prices);
				lock(sync)
				{
					completed++;
					Console.WriteLine($"Try: {completed} of {tries}, Price: {p} Count: {sol.Count}");
					if (p < bestPrice)
					{
						bestPrice = p;
						bestSol = sol;
					}
				}
			});

			return bestSol;
		}

		public static void AddRandom(List<Vector2i> buy, PriceMap prices, ProximityMap proxy, int seed, int hits, int findingRange, int minx = 0, int miny = 0, int maxx = Constants.Size, int maxy = Constants.Size)
		{
			Random mainRandom = new Random(seed);
			int[] seeds = new int[1];

			for(int i = 0; i < seeds.Length; i++)
			{
				seeds[i] = mainRandom.Next();
			}

			HashSet<Vector2i> finalBought = new HashSet<Vector2i>(buy);

			Parallel.For(0, seeds.Length, t =>
			{
				var r = new Random(seeds[t]);

				HashSet<Vector2i> bought = new HashSet<Vector2i>(buy);

				for (int i = 0; i < hits / seeds.Length; i++)
				{
					Vector2i pos = new Vector2i(r.Next(minx, maxx), r.Next(miny, maxy));
					pos = proxy.GetClosestBuilding(pos);
					var best = FindBest(prices, pos.X, pos.Y, findingRange);
					if (bought.Contains(best))
						continue;
					bought.Add(best);
				}

				lock(finalBought)
				{
					foreach(var b in bought)
					{
						if (!finalBought.Contains(b))
							finalBought.Add(b);
					}
				}
			});

			foreach(var b in finalBought)
			{
				buy.Add(b);
				//buildings.SetSquare(b.X, b.Y, false);
			}
		}

		public static void AddGrid(List<Vector2i> buy, PriceMap prices, ProximityMap proxy, int step, int findingRange)
		{
			var hash = new HashSet<Vector2i>(buy);
			for(int y = 0; y < Constants.Size; y += step)
			{
				for (int x = 0; x < Constants.Size; x += step)
				{
					Vector2i pos = new Vector2i(x, y);
					pos = proxy.GetClosestBuilding(pos);
					var best = FindBest(prices, pos.X, pos.Y, findingRange);
					if(hash.Contains(best))
					{
						continue;
					}
					hash.Add(best);
					buy.Add(best);
				}
			}
		}

		public static List<Vector2i> SolveRandom(PriceMap prices, Bitmap buildings, ProximityMap proxy, int seed, int hits, int findingRange, List<Vector2i> buy = null, int minx = 0, int miny = 0, int maxx = Constants.Size, int maxy = Constants.Size)
		{
			if (buy == null)
				buy = new List<Vector2i>();

			if(buildings != null)
			{
				Console.WriteLine("Using hungry solver");
				HungrySolver.SolveHungry(buy, prices, buildings);
			}
			
			//Console.WriteLine("Generating random points");
			AddRandom(buy, prices, proxy, seed, hits, findingRange, minx, miny, maxx, maxy);
			//AddGrid(buy, prices, proxy, 64, 64);

			//Console.WriteLine("Loading all other solutions");
			//var sols = Program.LoadSolutions(Environment.CurrentDirectory + "/best");
			//var set = new HashSet<Vector2i>(buy);
			//foreach(var sol in sols)
			//{
			//	foreach(var p in sol)
			//	{
			//		set.Add(p);
			//	}
			//}
			//buy = set.ToList();

			buy = Optimizer.GetOptimized(buy, prices, true, seed);

			return buy;
		}

		public static void SolveRolling(PriceMap prices, Bitmap buildings, ProximityMap proxy, int seed)
		{
			int cellSize = 1024;

			int cells = Constants.Size / cellSize;

			List<Vector2i> sol = Program.LoadSol("../../../../editsol_657918.out");

			HungrySolver.SolveHungry(sol, prices, buildings);

			long best = 660000;

			for(int cy = 0; cy < cells; cy++)
			{
				for(int cx = 0; cx < cells; cx++)
				{
					SolveRandom(prices, null, proxy, seed, hits: 1000, findingRange: 5, sol, cx * cellSize, cy * cellSize, (cx + 1) * cellSize, (cy + 1) * cellSize);
					seed++;
					long p = Program.GetPrice(sol, prices);
					if(p < best)
					{
						best = p;
						Console.WriteLine($"Found {p}");
						Program.SaveSolution("rolling", sol, p);
					}
				}
			}
		}
	}
}
