﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK.Mathematics;

namespace Solver
{
	class PriceMap
	{
		ushort[] _data;
		public int Width { get { return _size.X; } }
		public int Height { get { return _size.Y; } }
		Vector2i _size;

		public ushort this[int x, int y]
		{
			get
			{
				if (x < 0 || x >= Width)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= Height)
					throw new ArgumentOutOfRangeException(nameof(y));
				ushort d = _data[x + y * Width];
				if (d == 0)
					throw new Exception($"No building at coordinates {x}, {y}!");
				return _data[x + y * Width];
			}
			private set
			{
				if (x < 0 || x >= Width)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= Height)
					throw new ArgumentOutOfRangeException(nameof(y));
				_data[x + y * Width] = value;
			}
		}

		public PriceMap(int w, int h)
		{
			_size = new Vector2i(w, h);
			_data = new ushort[Width * Height];
		}

		public bool ExistsBuilding(int x, int y)
		{
			if (x < 0 || x >= Width)
				throw new ArgumentOutOfRangeException(nameof(x));
			if (y < 0 || y >= Height)
				throw new ArgumentOutOfRangeException(nameof(y));
			return _data[x + y * Width] > 0;
		}

		public static (PriceMap, Bitmap) FromFile(string file)
		{
			ushort min = ushort.MaxValue;
			ushort max = ushort.MinValue;

			using (var br = new BinaryReader(new FileStream(file, FileMode.Open)))
			{
				PriceMap prices = new PriceMap(Constants.Size, Constants.Size);
				Bitmap buildings = new Bitmap(Constants.Size, Constants.Size);
				for (int y = 0; y < Constants.Size; y++)
				{
					for (int x = 0; x < Constants.Size; x++)
					{
						ushort u = br.ReadUInt16();
						prices[x, y] = u;
						if(u > 0)
							min = Math.Min(min, u);
						max = Math.Max(max, u);
						if (u > 0)
						{
							buildings[x, y] = true;
						}
					}
				}

				Console.WriteLine($"Loaded. min: {min} max: {max}");

				return (prices, buildings);
			}
		}
	}
}
