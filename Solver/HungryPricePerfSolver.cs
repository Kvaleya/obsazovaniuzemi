﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Solver
{
	class HungryPricePerfSolver : ISolver
	{
		public string Name { get { return "hungrypp"; } }

		object _sync = new object();

		public int MaxIters;

		public HungryPricePerfSolver(int maxIters = 4)
		{
			MaxIters = maxIters;
		}

		// true if found anything
		bool FindBestPricePerf(int sx, int sy, int dx, int dy, PriceMap prices, Bitmap buildings, PrefixSum sum, out Vector2i result)
		{
			result = Vector2i.Zero;

			sx = Math.Max(sx, 0);
			sy = Math.Max(sy, 0);
			dx = Math.Min(dx, Constants.Size - 1);
			dy = Math.Min(dy, Constants.Size - 1);

			bool finished = true;

			double bestPricePerf = double.NegativeInfinity;
			Vector2i bestSpot = Vector2i.Zero;

			long getPrice(int x, int y)
			{
				if (x < 0 || y < 0 || x >= Constants.Size || y >= Constants.Size)
					return 0;
				//if (buildings[x, y])
					return prices[x, y];
				return 0;
			}

			double getPricePerf(int x, int y)
			{
				return sum.Sum(new Vector2i(x - Constants.Range, y - Constants.Range),
					new Vector2i(x + Constants.Range, y + Constants.Range))
					/ (double)getPrice(x, y);
			}

			System.Threading.Tasks.Parallel.For(sy, dy+1, y =>
			{
				double localBest = double.NegativeInfinity;
				Vector2i localSpot = Vector2i.Zero;
				bool localFinished = false;
				for (int x = sx; x < dx + 1; x++)
				{
					if (prices.ExistsBuilding(x, y))
					{
						localFinished = false;

						// Evaluate
						double pricePerf = getPricePerf(x, y);

						if (pricePerf > localBest)
						{
							localBest = pricePerf;
							localSpot = new Vector2i(x, y);
						}
					}
				}

				lock (_sync)
				{
					finished = finished && localFinished;
					if (localBest > bestPricePerf)
					{
						bestPricePerf = localBest;
						bestSpot = localSpot;
					}
				}
			});

			if (finished)
			{
				return false;
			}
			else
			{
				result = bestSpot;
				return true;
			}
		}

		public List<Vector2i> Solve(PriceMap prices, Bitmap buildings)
		{
			ProximityMap proxy = new ProximityMap(buildings);

			var buy = new List<Vector2i>();

			PrefixSum sum = new PrefixSum(prices, buildings);

			long pricesum = 0;

			long buildingCount = buildings.TotalSet;

			while (buy.Count < MaxIters)
			{
				Console.WriteLine($"PP iter {buy.Count + 1}");
				
				Vector2i result;
				bool found = FindBestPricePerf(0, 0, Constants.Size - 1, Constants.Size - 1, prices, buildings, sum, out result);
				
				if(!found)
				{
					break;	
				}

				buy.Add(result);
				buildings.SetSquare(result.X, result.Y, false);
				pricesum += prices[result.X, result.Y];

				int startX = result.X - Constants.Range - 1;
				int startY = result.Y - Constants.Range - 1;

				if (startX < 0)
					startX = 0;
				if (startY < 0)
					startY = 0;

				Console.WriteLine($"Fixing sum, price: {pricesum}; so far covered: {(buildingCount - buildings.TotalSet) / (double)buildingCount}");
				sum.Compute(prices, buildings, startX, startY);
			}

			// Finish using hungry
			Console.WriteLine("Finishing using hungry PP");

			int up = 0, down = 0, left = 0, right = 0;
			int x = 0, y = 0;
			int dirx = 1, diry = 0;
			/*
			for(int i = 0; i < Constants.Size * Constants.Size; i++)
			//for(int y = 0; y < Constants.Size; y++)
			{
				//Console.WriteLine($"x = {x} y = {y} dx = {dirx} dy = {diry} i = {i}");
				//for (int x = 0; x < Constants.Size; x++)
				try
				{
					if(buildings[x, y])
					{
						Vector2i result;
						bool found = FindBestPricePerf(x - Constants.Range, y - Constants.Range,
							x + Constants.Range, y + Constants.Range, prices, buildings, sum, out result);

						if (!found)
						{
							result = new Vector2i(x, y);
						}

						buy.Add(result);
						buildings.SetSquare(result.X, result.Y, false);
						pricesum += prices[result.X, result.Y];

						int startX = result.X - Constants.Range - 1;
						int startY = result.Y - Constants.Range - 1;

						if (startX < 0)
							startX = 0;
						if (startY < 0)
							startY = 0;

						Console.WriteLine($"It: {i / (double)(Constants.Size * Constants.Size)} Buy: x: {result.X} y: {result.Y} Fixing sum, price: {pricesum}; so far covered: {(buildingCount - buildings.TotalSet) / (double)buildingCount}");
						sum.Compute(prices, buildings, startX, startY);
					}
				}
				catch(Exception e)
				{
					Console.WriteLine($"Ex: x = {x} y = {y} dx = {dirx} dy = {diry} i = {i}");
					throw e;
				}

				// Move in a spiral
				int nx = x + dirx;
				int ny = y + diry;

				if (nx < left && dirx == -1)
				{
					//Console.WriteLine("Go up");
					down++;
					dirx = 0;
					diry = -1;
				}
				else if(ny < up && diry == -1)
				{
					//Console.WriteLine("Go right");
					left++;
					dirx = 1;
					diry = 0;
				}
				else if(nx >= Constants.Size - right && dirx == 1)
				{
					//Console.WriteLine("Go down");
					up++;
					dirx = 0;
					diry = 1;
				}
				else if(ny >= Constants.Size - down && diry == 1)
				{
					//Console.WriteLine("Go left");
					right++;
					dirx = -1;
					diry = 0;
				}

				x += dirx;
				y += diry;
			}
			*/

			Console.WriteLine("Populating with random points");
			RandomSolver.AddRandom(buy, prices, proxy, 420, 16384, 64);
			Console.WriteLine("Populating with grid");
			RandomSolver.AddGrid(buy, prices, proxy, 200, 64);
			Console.WriteLine("Optimizing");
			//return buy;
			return Optimizer.GetOptimized(buy, prices);
		}
	}
}
