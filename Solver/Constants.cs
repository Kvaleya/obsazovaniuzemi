﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Solver
{
	static class Constants
	{
		public const int SizePow = 14; // 2^14 = 16384
		public const int Size = 1 << SizePow;
		public const int Range = 500;
	}
}
