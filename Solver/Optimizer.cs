﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenTK.Mathematics;

namespace Solver
{
	class Optimizer
	{
		const int MaxRunningOptimizers = 3;

		static Semaphore _sem = new Semaphore(MaxRunningOptimizers, MaxRunningOptimizers);

		public static List<Vector2i> GetOptimized(List<Vector2i> sol, PriceMap prices, bool randomizeOrder = false, int seed = 0)
		{
			_sem.WaitOne();

			//Console.WriteLine($"Optimizer started for {sol.Count} priced {Program.GetPrice(sol, prices)}");
			//Console.WriteLine("Computing overbuy");

			OverbuyMap overbuy;

			try
			{
				overbuy = new OverbuyMap(prices, sol);
			}
			catch (ArgumentOutOfRangeException e)
			{
				Console.WriteLine("Optimized failed: too much overbuy (>65535), returning unoptimized solution.");
				_sem.Release();
				return sol.ToList();
			}

			//Console.WriteLine("Removing redundants");
			List<Vector2i> newsol = WithRemovedRedundant(sol, prices, overbuy, randomizeOrder, seed);
			//Console.WriteLine($"Optimized to {Program.GetPrice(newsol, prices)}");
			/*
			Console.WriteLine("Shifting");
			ShiftOptimize(newsol, overbuy, prices);
			Console.WriteLine($"Optimized to {Program.GetPrice(newsol, prices)}");
			*/
			//Console.WriteLine("Done");

			_sem.Release();
			return newsol;
		}

		public static List<Vector2i> WithRemovedRedundant(List<Vector2i> sol, PriceMap prices, OverbuyMap overbuy, bool randomizeOrder, int seed)
		{
			var ordered = sol.OrderByDescending(p => prices[p.X, p.Y]).ToList();

			if(randomizeOrder)
			{
				const int range = 20;
				Random r = new Random(seed);
				for(int i = 0; i < ordered.Count; i++)
				{
					int newpos = i + r.Next(-range, range + 1);
					if (newpos < 0 || newpos == i || newpos >= ordered.Count)
						continue;
					var tmp = ordered[i];
					ordered[i] = ordered[newpos];
					ordered[newpos] = tmp;
				}
			}

			bool[] remove = new bool[sol.Count];

			for(int i = 0; i < ordered.Count; i++)
			{
				var p = ordered[i];

				if (overbuy.FindMin(p) > 1)
				{
					remove[i] = true;
					overbuy.Add(p, -1);
				}
			}

			var newsol = new List<Vector2i>();

			for(int i = 0; i < ordered.Count; i++)
			{
				if(!remove[i])
				{
					newsol.Add(ordered[i]);
				}
			}

			//Console.WriteLine($"Now {newsol.Count} of {sol.Count}, redundant remover done!");

			return newsol.ToList();
		}

		public static void ShiftOptimize(List<Vector2i> sol, OverbuyMap overbuy, PriceMap prices)
		{
			var sum = Program.GetPrice(sol, prices);

			while (true)
			{
				bool improved = false;

				for(int i = 0; i < sol.Count; i++)
				{
					Console.Write('.');
					// Try to find a better placement for this house
					var p = sol[i];

					// Remove this house
					overbuy.Add(p, -1);

					Vector2i lowestsPoint = p;
					int lowestPrice = prices[p.X, p.Y];

					for(int y = -Constants.Range; y <= Constants.Range; y++)
					{
						for (int x = -Constants.Range; x <= Constants.Range; x++)
						{
							Vector2i here = new Vector2i(p.X + x, p.Y + y);

							if (!prices.ExistsBuilding(here.X, here.Y))
								continue;

							overbuy.Add(here, 1);
							int price = prices[here.X, here.Y];

							if(price < lowestPrice && overbuy.FindMin(p) > 0)
							{
								lowestPrice = price;
								lowestsPoint = here;
							}

							overbuy.Add(here, -1);
						}
					}

					overbuy.Add(lowestsPoint, 1);

					if (lowestsPoint != p)
					{
						improved = true;
						sum -= prices[p.X, p.Y];
						sum += lowestPrice;
						sol[i] = lowestsPoint;
						break;
					}
				}

				if(improved)
				{
					Console.WriteLine($"Improved to {sum}");
				}
				else
				{
					break;
				}
			}

			Console.WriteLine("Shift optimizer done");
		}
	}
}
