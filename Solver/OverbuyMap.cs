﻿using System;
using System.Collections.Generic;
using OpenTK.Mathematics;

namespace Solver
{
	class OverbuyMap
	{
		//QuadTreeNode _tree;
		PriceMap _prices;
		ushort[] _data;

		public OverbuyMap(PriceMap prices)
		{
			_prices = prices;
			_data = new ushort[Constants.Size * Constants.Size];
			//_tree = QuadTreeNode.Create();
		}

		public OverbuyMap(PriceMap prices, List<Vector2i> sol)
			:this(prices)
		{
			foreach(var p in sol)
			{
				Add(p.X, p.Y, 1);
			}
		}

		public int FindMin(Vector2i pos)
		{
			return FindMin(pos.X, pos.Y);
		}

		public int FindMin(int cx, int cy)
		{
			int min = int.MaxValue;
			for (int y = cy - Constants.Range; y <= cy + Constants.Range; y++)
			{
				if (y < 0 || y >= Constants.Size)
					continue;

				for (int x = cx - Constants.Range; x <= cx + Constants.Range; x++)
				{
					if (x < 0 || x >= Constants.Size)
						continue;
					if (!_prices.ExistsBuilding(x, y))
						continue; // Only consider buildings
					min = Math.Min(min, _data[x + y * Constants.Size]);
				}
			}
			return min;
		}

		public void Add(Vector2i pos, int add)
		{
			Add(pos.X, pos.Y, add);
		}

		public void Add(int cx, int cy, int add)
		{
			for (int y = cy - Constants.Range; y <= cy + Constants.Range; y++)
			{
				if (y < 0 || y >= Constants.Size)
					continue;

				for (int x = cx - Constants.Range; x <= cx + Constants.Range; x++)
				{
					if (x < 0 || x >= Constants.Size)
						continue;
					int index = x + y * Constants.Size;
					int computed = (int)_data[index] + add;
					if (computed >= ushort.MaxValue)
					{
						throw new ArgumentOutOfRangeException();
					}
					_data[index] = (ushort)computed;
				}
			}
		}
	}
}
