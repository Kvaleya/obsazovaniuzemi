﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK.Mathematics;

namespace Solver
{
	class ProximityMap
	{
		const byte None = 0;
		const byte Here = 1;
		const byte Up = 2;
		const byte Right = 3;
		const byte Down = 4;
		const byte Left = 5;

		byte[] _data;
		public int Width { get { return _size.X; } }
		public int Height { get { return _size.Y; } }
		Vector2i _size;

		private byte this[int x, int y]
		{
			get
			{
				if (x < 0 || x >= Width)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= Height)
					throw new ArgumentOutOfRangeException(nameof(y));
				return _data[x + y * Width];
			}
			set
			{
				if (x < 0 || x >= Width)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= Height)
					throw new ArgumentOutOfRangeException(nameof(y));
				_data[x + y * Width] = value;
			}
		}

		const string MapFile = "proximity.bin";

		public ProximityMap(Bitmap buildings)
		{
			_size = new Vector2i(buildings.Width, buildings.Height);
			_data = new byte[Width * Height];
			if(File.Exists(MapFile))
			{
				Console.WriteLine("Loading proxymity map");
				using (var br = new BinaryReader(File.OpenRead(MapFile)))
				{
					_data = br.ReadBytes(_data.Length);
				}
			}
			else
			{
				Init(buildings);
				Console.WriteLine("Saving proxymity map");
				using (var bw = new BinaryWriter(File.OpenWrite(MapFile)))
				{
					bw.Write(_data);
				}
			}
		}

		public Vector2i GetClosestBuilding(Vector2i pos)
		{
			byte value = this[pos.X, pos.Y];

			while(value != Here)
			{
				if(value == Right)
				{
					pos.X++;
				}
				if (value == Left)
				{
					pos.X--;
				}
				if (value == Down)
				{
					pos.Y++;
				}
				if (value == Up)
				{
					pos.Y--;
				}

				value = this[pos.X, pos.Y];
			}

			return pos;
		}

		void Init(Bitmap buildings)
		{
			uint pack(int x, int y)
			{
				return ((uint)x & 0xffff) | (((uint)y & 0xffff) << 16);
			}

			Vector2i unpack(uint packed)
			{
				return new Vector2i((int)(packed & 0xffff), (int)((packed >> 16) & 0xffff));
			}

			Queue<uint> queue = new Queue<uint>();

			for(int y = 0; y < Height; y++)
			{
				for(int x = 0; x < Width; x++)
				{
					if (!buildings[x, y])
						continue;
					queue.Enqueue(pack(x, y));
					this[x, y] = Here;
				}
			}

			Console.WriteLine("Generating proximity map");

			int its = 0;

			while(queue.Count > 0)
			{
				if(its % 64000000 == 0)
				{
					Console.WriteLine($"{its} / {buildings.Width * buildings.Height}");
				}
				its++;

				Vector2i here = unpack(queue.Dequeue());

				for(int i = 0; i < 4; i++)
				{
					int x = here.X;
					int y = here.Y;
					byte dir = Here;

					if(i == 0)
					{
						x++;
						dir = Left;
					}
					if (i == 1)
					{
						x--;
						dir = Right;
					}
					if (i == 2)
					{
						y++;
						dir = Up;
					}
					if (i == 3)
					{
						y--;
						dir = Down;
					}

					if(x < 0 || y < 0 || x >= Width || y >= Height || this[x, y] != None)
					{
						continue;
					}

					this[x, y] = dir;
					queue.Enqueue(pack(x, y));
				}
			}
		}
	}
}
