﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Solver
{
	class QuadTreeNode
	{
		// TODO: min mě zajímá jen tam kde jsou budovy, co s tím?
		// TODO: je to rozbité
		const int MaxDepth = Constants.SizePow;

		public int X => (int)(_packedCoords & 0x3fff);
		public int Y => (int)((_packedCoords >> 14) & 0x3fff);
		/// <summary>
		/// Reverse depth. Is 0 for bottom level (size=0) nodes, is 14 for top level (16384) node
		/// </summary>
		public int Height => (int)((_packedCoords >> 28) & 0xf);
		public int Size => 1 << Height;
		public int Area => Size * Size;
		/// <summary>
		/// Exclusive right edge of node
		/// </summary>
		public int Xmax => X + Size;
		/// <summary>
		/// Exclusive bottom edge of node
		/// </summary>
		public int Ymax => Y + Size;

		public bool HasValue => _packedValue > -1;

		/// <summary>
		/// Autoupdates min, max and sum on set
		/// </summary>
		public int Value
		{
			get
			{
				if (!HasValue)
					throw new InvalidOperationException("Node has no value!");
				return _packedValue;
			}
			private set
			{
				int old = _packedValue;
				_packedValue = value;
				if (!HasValue)
				{
					_packedValue = old;
					throw new InvalidOperationException("This value cannot be assigned!");
				}
				Min = _packedValue;
				Max = _packedValue;
				Sum = (long)_packedValue * Area;
			}
		}

		public int Min, Max;
		public long Sum;
		uint _packedCoords;
		int _packedValue;

		// 01
		// 23
		QuadTreeNode[] _children = null;

		private QuadTreeNode(int x, int y, int height, int value)
		{
			Value = value;
			PackCoords(x, y, height);
		}

		void PackCoords(int x, int y, int height)
		{
			_packedCoords = 0;
			_packedCoords |= (uint)x & 0x3fff;
			_packedCoords |= ((uint)y & 0x3fff) << 14;
			_packedCoords |= ((uint)height & 0xf) << 28;
		}

		void Subdivide()
		{
			if (!HasValue || _children != null)
			{
				throw new InvalidOperationException("Node should already be subdivided!");
			}
			if (Height == 0)
			{
				throw new InvalidOperationException("Cannot subdivide bottom level node!");
			}

			_children = new QuadTreeNode[4];
			int h = Height - 1;
			int s = Size / 2;
			_children[0] = new QuadTreeNode(X, Y, h, Value);
			_children[1] = new QuadTreeNode(X + s, Y, h, Value);
			_children[2] = new QuadTreeNode(X, Y + s, h, Value);
			_children[3] = new QuadTreeNode(X + s, Y + s, h, Value);
			// This node no longer has value
			_packedValue = -1;
		}

		public static QuadTreeNode Create(int value)
		{
			return new QuadTreeNode(0, 0, MaxDepth, value);
		}

		/// <summary>
		/// Query a rectangle. Coordinates are exclusive.
		/// </summary>
		/// <param name="x1">Left</param>
		/// <param name="y1">Top</param>
		/// <param name="x2">Right</param>
		/// <param name="y2">Bottom</param>
		public bool Query(int x1, int y1, int x2, int y2, out int min, out int max, out long sum)
		{
			min = int.MaxValue;
			max = int.MinValue;
			sum = 0;

			if (x2 <= X || y2 <= Y || x1 >= Xmax || y1 >= Ymax)
			{
				// Out of bounds of this node
				return false;
			}

			if (HasValue)
			{
				min = Min;
				max = Max;

				// Clamp query rectangle to this cell
				x1 = Math.Max(x1, X);
				y1 = Math.Max(y1, Y);
				x2 = Math.Min(x2, Xmax);
				y2 = Math.Min(y2, Ymax);
				sum = (long)Value * (x2 - x1) * (y2 - y1);

				return true;
			}

			foreach (var c in _children)
			{
				int cmin, cmax;
				long csum;
				c.Query(x1, y1, x2, y2, out cmin, out cmax, out csum);
				min = Math.Min(min, cmin);
				max = Math.Max(max, cmax);
				sum += csum;
			}

			return true;
		}

		public void Add(int x1, int y1, int x2, int y2, int add)
		{
			if (x2 <= X || y2 <= Y || x1 >= Xmax || y1 >= Ymax)
			{
				// Out of bounds of this node
				return;
			}

			if (HasValue)
			{
				if (x1 >= X && y1 >= Y && x2 <= Xmax && y2 <= Ymax)
				{
					// Node is fully inside add area
					Value = Value + add;
					return;
				}

				// Node intersects the area but does not lie fully inside
				Subdivide();
				// This node no longer has value. Continue.
			}

			// At this point, node must have children

			// Recurse
			foreach (var c in _children)
			{
				c.Add(x1, y1, x2, y2, add);
			}

			// Update min, max, sum
			Min = int.MaxValue;
			Max = int.MinValue;
			Sum = 0;
			foreach (var c in _children)
			{
				Min = Math.Min(Min, c.Min);
				Max = Math.Max(Max, c.Max);
				Sum += c.Sum;
			}

			// Try to merge children into a single node

			if (!_children[0].HasValue)
			{
				// Abort if a child has no value
				return;
			}

			int cvalue = _children[0].Value;
			for (int i = 1; i < 4; i++)
			{
				if (!_children[i].HasValue || _children[i].Value != cvalue)
				{
					// Abort if a child has no value or the value differs
					return;
				}
			}

			// Merge children
			this.Value = cvalue;
			this._children = null;
		}
	}
}
