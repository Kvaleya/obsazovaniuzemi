﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Solver
{
	public class Bitmap
	{
		uint[] _data;
		public int Width { get { return _size.X; } }
		public int Height { get { return _size.Y; } }
		int _rowInts;
		Vector2i _size;

		public long TotalSet { get; private set; } = 0;

		public bool this[int x, int y]
		{
			get
			{
				if (x < 0 || x >= Width)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= Height)
					throw new ArgumentOutOfRangeException(nameof(y));
				int index = y * _rowInts + (x / 32);
				uint bit = (uint)1 << (x % 32);
				return (_data[index] & bit) != 0;
			}
			set
			{
				if (x < 0 || x >= Width)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= Height)
					throw new ArgumentOutOfRangeException(nameof(y));
				int index = y * _rowInts + (x / 32);
				uint bit = (uint)1 << (x % 32);

				if(((_data[index] & bit) != 0) != value)
				{
					if (value)
					{
						TotalSet++;
						_data[index] = _data[index] | bit;
					}
					else
					{
						TotalSet--;
						_data[index] = _data[index] & (~bit);
					}
				}
			}
		}

		public Bitmap(int w, int h)
		{
			_size = new Vector2i(w, h);
			_rowInts = (w + 31) / 32;
			_data = new uint[_rowInts * h];
		}

		public Bitmap Duplicate()
		{
			Bitmap b = new Bitmap(Width, Height);
			for(int i = 0; i < _data.Length; i++)
			{
				b._data[i] = _data[i];
			}
			b.TotalSet = TotalSet;
			return b;
		}

		public int SetSquare(int cx, int cy, bool value)
		{
			int changed = 0;

			for(int y = cy - Constants.Range; y <= cy + Constants.Range; y++)
			{
				if(y < 0 || y >= Height)
				{
					continue;
				}
				for (int x = cx - Constants.Range; x <= cx + Constants.Range; x++)
				{
					if (x < 0 || x >= Width)
					{
						continue;
					}
					if (this[x, y] != value)
					{
						this[x, y] = value;
						changed++;
					}
				}
			}

			return changed;
		}
	}
}
