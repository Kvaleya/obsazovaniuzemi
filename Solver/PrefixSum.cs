﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Solver
{
	class PrefixSum
	{
		long[] _data;
		public int Width { get { return _size.X; } }
		public int Height { get { return _size.Y; } }
		Vector2i _size;

		private long this[int x, int y]
		{
			get
			{
				if (x < 0 || x >= Width)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= Height)
					throw new ArgumentOutOfRangeException(nameof(y));
				return _data[x + y * Width];
			}
			set
			{
				if (x < 0 || x >= Width)
					throw new ArgumentOutOfRangeException(nameof(x));
				if (y < 0 || y >= Height)
					throw new ArgumentOutOfRangeException(nameof(y));
				_data[x + y * Width] = value;
			}
		}

		public PrefixSum(PriceMap prices, Bitmap buildings)
		{
			_size = new Vector2i(prices.Width, prices.Height);
			_data = new long[Width * Height];
			Compute(prices, buildings);
		}

		long Fetch(Vector2i v)
		{
			return Fetch(v.X, v.Y);
		}

		long Fetch(int x, int y)
		{
			if (x < 0 || y < 0)
				return 0;
			x = Math.Min(x, Width - 1);
			y = Math.Min(y, Height - 1);
			return this[x, y];
		}

		public long Sum(Vector2i minInclusive, Vector2i maxInclusive)
		{
			return Fetch(maxInclusive) - Fetch(minInclusive.X - 1, maxInclusive.Y) - Fetch(maxInclusive.X, minInclusive.Y - 1)
				+ Fetch(minInclusive - Vector2i.One);
		}

		public void Compute(PriceMap prices, Bitmap buildings, int startX = 0, int startY = 0)
		{
			for(int y = startY; y < Height; y++)
			{
				for(int x = startX; x < Width; x++)
				{
					long local = 0;
					if (buildings[x, y])
						local = prices[x, y];
					this[x, y] = Fetch(x-1,y) + Fetch(x, y-1) - Fetch(x-1 ,y-1) + local;
				}
			}
		}
	}
}
