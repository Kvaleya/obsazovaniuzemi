﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Solver
{
	class SimpleSolver : ISolver
	{
		public string Name { get { return "simple"; } }

		public List<Vector2i> Solve(PriceMap prices, Bitmap buildings)
		{
			List<Vector2i> buy = new List<Vector2i>();

			for (int y = 0; y < prices.Height; y++)
			//for (int y = prices.Height-1; y >= 0; y--)
			{
				//for (int x = 0; x < prices.Width; x++)
				for (int x = prices.Width-1; x >= 0; x--)
				{
					if (buildings[x, y])
					{
						buy.Add(new Vector2i(x, y));
						buildings.SetSquare(x, y, false);
					}
				}
			}

			return buy;
		}
	}
}
