﻿using System;

namespace Editor
{
	class Program
	{
		static void Main(string[] args)
		{
			Editor ed = new Editor();
			ed.Run();
		}
	}
}
