﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK.Mathematics;
using ImGuiNET;
using GlobCore;
using OpenTK.Graphics.OpenGL;
using OpenTK.Windowing.GraphicsLibraryFramework;
using System.Drawing;

namespace Editor
{
	class Editor
	{
		public ViewWindow Window { get; private set; }
		public ImGuiRenderer ImGuiRenderer;
		Stopwatch _stopwatch = new Stopwatch();
		double _lastEllapsed = 0;
		double _currentEllapsed = 0;
		double _currentDeltaT = 0;
		private double GetEllapsed() { return _stopwatch.ElapsedTicks / (double)Stopwatch.Frequency; }
		public TextOutput TextOutput;

		const int DataSize = 16384;

		ushort[] _data;
		Texture2D _texDatasetUnorm;
		Texture2D _texCoverage;
		Texture2D _texColors;
		int _bufferHouses = 0;

		public Device Device { get { return Window.Device; } }

		GraphicsPipeline _psoDrawDataset;
		GraphicsPipeline _psoDrawHouses;
		ComputePipeline _psoClearCoverage;
		ComputePipeline _psoGenCoverage;
		ComputePipeline _psoGenColors;
		ComputePipeline _psoGenMip;
		ComputePipeline _psoMarkBought;

		Vector2 _center = new Vector2(0.5f);
		float _zoom = 1;

		Vector2 _viewMin, _viewMax;

		List<Vector2i> _sol = new List<Vector2i>();
		List<Vector2i> _sol2 = new List<Vector2i>();
		long _solPrice = 0;
		bool _valid = false;
		bool _visualizePriceDif = false;
		bool _dispalySecondary = false;
		ushort _clickedPrice = 0;
		ushort _hoveredPrice = 0;

		public void Run()
		{
			TextOutput = new TextOutput();

			Window = new ViewWindow(() => this.UpdateAndDrawFrame(), (w, h) => this.OnResize(w, h), TextOutput);

			ImGuiRenderer = new ImGuiRenderer(Window, Window.Device);
			ImGuiRenderer.RebuildFontAtlas();

			Console.WriteLine("Loading dataset");
			_data = LoadDataset("01.in");

			_texColors = new Texture2D(Device, "TexColors", SizedInternalFormatGlob.RGBA8, DataSize, DataSize, 0);
			GlobCore.Utils.SetTextureParameters(Device, _texColors, TextureWrapMode.ClampToBorder, TextureMagFilter.Nearest, TextureMinFilter.LinearMipmapLinear);

			_texCoverage = new Texture2D(Device, "TexCoverage", SizedInternalFormatGlob.R32I, DataSize, DataSize, 1);

			_texDatasetUnorm = new Texture2D(Device, "TexDataset", SizedInternalFormatGlob.R16, DataSize, DataSize, 0);
			_texDatasetUnorm.TexSubImage2D(Device, 0, 0, 0, DataSize, DataSize, PixelFormat.Red, PixelType.UnsignedShort, _data);
			GL.GenerateTextureMipmap(_texDatasetUnorm.Handle);
			GlobCore.Utils.SetTextureParameters(Device, _texDatasetUnorm, TextureWrapMode.ClampToBorder, TextureMagFilter.Nearest, TextureMinFilter.LinearMipmapLinear);

			Device.GlobalUniforms.SetTexture("g_tex_dataset", _texDatasetUnorm);
			Device.GlobalUniforms.SetTexture("g_tex_color", _texColors);
			Device.GlobalUniforms.SetTexture("g_tex_coverage", _texCoverage);

			_psoDrawDataset = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("dataset.frag"), null, new RasterizerState(), new DepthState());
			_psoDrawHouses = new GraphicsPipeline(Device, Device.GetShader("point.vert"), Device.GetShader("yellow.frag"), null, new RasterizerState(), new DepthState());

			_psoClearCoverage = new ComputePipeline(Device, Device.GetShader("clearImage.comp"));
			_psoGenCoverage = new ComputePipeline(Device, Device.GetShader("genCov.comp"));
			_psoGenColors = new ComputePipeline(Device, Device.GetShader("genColors.comp"));
			_psoGenMip = new ComputePipeline(Device, Device.GetShader("genMip.comp"));
			_psoMarkBought = new ComputePipeline(Device, Device.GetShader("markBought.comp"));

			UpdateAll();

			_stopwatch.Start();
			Window.Size = new Vector2i(1280, 720);
			Window.Run();
		}

		void UpdateAll()
		{
			_solPrice = 0;
			_sol = new HashSet<Vector2i>(_sol).ToList(); // Remove redundant using hashing

			// Sanitize sol and compute price
			for(int i = 0; i < _sol.Count; i++)
			{
				Vector2i s = _sol[i];

				bool kill = false;

				if (s.X < 0 || s.Y < 0 || s.X >= DataSize || s.Y >= DataSize)
					kill = true;

				if(!kill)
				{
					ushort price = _data[_sol[i].X + _sol[i].Y * DataSize];

					if(price == 0)
					{
						kill = true;
					}
					else
					{
						_solPrice += price;
					}
				}
				
				if(kill)
				{
					_sol.RemoveAt(i);
					i--;
				}
			}

			if (_bufferHouses > 0)
				GL.DeleteBuffer(_bufferHouses);
			_bufferHouses = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(_sol.Count * 8), _sol.ToArray(), BufferStorageFlags.None, "BufferHouses");

			// clear coverage
			Device.BindPipeline(_psoClearCoverage);
			Device.BindImage2D(0, _texCoverage, TextureAccess.WriteOnly);
			Device.DispatchComputeThreads(_texCoverage);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			// mark coverage
			Device.BindPipeline(_psoGenCoverage);
			Device.BindImage2D(0, _texCoverage, TextureAccess.ReadWrite);

			foreach (var point in _sol)
			{
				Device.ShaderCompute.SetUniformI("center", point);
				Device.DispatchComputeThreads(1024, 1024);
			}

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			// gen colors
			int buf = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(4), new int[] { 0 }, BufferStorageFlags.MapReadBit, "BufferValid");

			Device.BindPipeline(_psoGenColors);
			Device.BindImage2D(0, _texColors, TextureAccess.WriteOnly);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, buf);
			Device.BindGlobalUniforms(Device.ShaderCompute);
			Device.DispatchComputeThreads(_texColors);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit
				| MemoryBarrierFlags.TextureFetchBarrierBit
				| MemoryBarrierFlags.ClientMappedBufferBarrierBit
				| MemoryBarrierFlags.BufferUpdateBarrierBit);

			GL.Finish(); // PRASÁRNA!!!

			IntPtr ptr = GL.MapNamedBuffer(buf, BufferAccess.ReadOnly);
			_valid = Marshal.ReadInt32(ptr) == 0;
			GL.UnmapNamedBuffer(buf);
			
			GL.DeleteBuffer(buf);

			// mark bought
			//Device.BindPipeline(_psoMarkBought);
			//Device.BindImage2D(0, _texColors, TextureAccess.ReadWrite);

			//foreach (var point in _sol)
			//{
			//	Device.ShaderCompute.SetUniformI("center", point);
			//	Device.DispatchComputeThreads(1, 1);
			//	GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
			//}

			// gen mips
			GenMips();
		}

		void GenMips()
		{
			Device.BindPipeline(_psoGenMip);

			for (int i = 1; i < _texColors.Levels; i++)
			{
				Device.BindImage2D(0, _texColors, TextureAccess.ReadOnly, i - 1);
				Device.BindImage2D(1, _texColors, TextureAccess.WriteOnly, i);
				Device.DispatchComputeThreads(_texColors.Width >> i, _texColors.Height >> i);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
			}
		}

		void ExportImage()
		{
			// mark bought
			Device.BindPipeline(_psoMarkBought);
			Device.BindImage2D(0, _texColors, TextureAccess.ReadWrite);

			foreach (var point in _sol)
			{
				Device.ShaderCompute.SetUniformI("center", point);
				Device.DispatchComputeThreads(128, 128);
			}

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			GenMips();

			GL.Finish();

			string file = $"image_{_solPrice}";

			{
				Bitmap bm = new Bitmap(DataSize, DataSize);

				var bits = bm.LockBits(new Rectangle(0, 0, DataSize, DataSize),
					System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				{
					Device.BindTexture(TextureTarget.Texture2D, _texColors.Handle);
					GL.GetTexImage(TextureTarget.Texture2D, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bits.Scan0);
					Device.BindTexture(TextureTarget.Texture2D, 0);
				}
				bm.UnlockBits(bits);
				bm.Save(file + ".png", System.Drawing.Imaging.ImageFormat.Png);
			}

			{
				int level = 3;
				Bitmap bm = new Bitmap(DataSize >> level, DataSize >> level);

				var bits = bm.LockBits(new Rectangle(0, 0, DataSize >> level, DataSize >> level),
					System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				{
					Device.BindTexture(TextureTarget.Texture2D, _texColors.Handle);
					GL.GetTexImage(TextureTarget.Texture2D, level, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bits.Scan0);
					Device.BindTexture(TextureTarget.Texture2D, 0);
				}
				bm.UnlockBits(bits);
				bm.Save("sm_" + file + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
			}
		}

		public void OnResize(int w, int h)
		{
			Device.GlobalUniforms.SetUniformI("g_viewport", w, h);
		}

		public void UpdateAndDrawFrame()
		{
			_currentEllapsed = GetEllapsed();
			_currentDeltaT = _currentEllapsed - _lastEllapsed;
			_lastEllapsed = _currentEllapsed;

			bool acceptControls = true;
			
			Vector2 mouse = new Vector2(Window.MouseState.X / (float)Window.Size.X, (Window.Size.Y - Window.MouseState.Y) / (float)Window.Size.Y);
			Vector2 mouseDelta = new Vector2(Window.MouseState.Delta.X / Window.Size.X, Window.MouseState.Delta.Y / Window.Size.Y);

			if (acceptControls)
			{
				// Movement
				_zoom += Window.MouseState.ScrollDelta.Y * 0.5f;
				const float zoomMin = 0.1f;
				const float zoomMax = 18.0f;

				if (_zoom < zoomMin)
					_zoom = zoomMin;
				if (_zoom > zoomMax)
					_zoom = zoomMax;

				if (Window.KeyboardState.IsKeyDown(Keys.Space))
				{
					_center += mouseDelta * -1 * (float)Math.Pow(2.0, -_zoom) * 4;
				}

				_center = Vector2.Clamp(_center, Vector2.Zero, Vector2.One);
			}

			// Calculate view region
			{
				float side = (float)Math.Pow(2.0, -_zoom);
				var region = new Vector2(side * Window.Size.X / Window.Size.Y, -side);
				_viewMin = _center - region;
				_viewMax = _center + region;
				Device.GlobalUniforms.SetUniformF("g_view_minmax", new Vector4(_viewMin.X, _viewMin.Y, _viewMax.X, _viewMax.Y));
			}

			Vector2 hovered = _viewMin + mouse * (_viewMax - _viewMin);
			Vector2i hoveredHouse = new Vector2i((int)Math.Floor(hovered.X * DataSize), (int)Math.Floor(hovered.Y * DataSize));
			hoveredHouse = Vector2i.Clamp(hoveredHouse, new Vector2i(0), new Vector2i(DataSize - 1));

			var hovprice = _data[hoveredHouse.X + hoveredHouse.Y * DataSize];
			if(hovprice > 0)
			{
				_hoveredPrice = hovprice;
			}

			Device.GlobalUniforms.SetUniformF("g_hovered_price", _hoveredPrice / 65535f);

			if(Window.MouseState.IsButtonDown(MouseButton.Button1))
			{
				_clickedPrice = _hoveredPrice;
				Device.GlobalUniforms.SetUniformF("g_clicked_price", _clickedPrice / 65535f);
			}

			if(Window.KeyboardState.IsKeyPressed(Keys.R))
			{
				Vector2i closest = new Vector2i(-10000000);
				int closestDist = 100000000;

				foreach(var h in _sol)
				{
					int dist = Math.Max(Math.Abs(h.X - hoveredHouse.X), Math.Abs(h.Y - hoveredHouse.Y));
					if(dist < closestDist)
					{
						closestDist = dist;
						closest = h;
					}
				}

				if(closestDist < 100)
				{
					_sol.Remove(closest);
					UpdateAll();
				}
			}

			if(Window.KeyboardState.IsKeyPressed(Keys.B))
			{
				if(_hoveredPrice > 0 && !_sol.Contains(hoveredHouse))
				{
					_sol.Add(hoveredHouse);
					UpdateAll();
				}
			}

			const int minbuyrange = 10;

			if (Window.KeyboardState.IsKeyPressed(Keys.M))
			{
				ushort cheapestPrice = 65535;
				Vector2i cheapest = new Vector2i(-1);

				for(int y = Math.Max(hoveredHouse.Y - minbuyrange, 0); y <= hoveredHouse.Y + minbuyrange && y < DataSize; y++)
				{
					for (int x = Math.Max(hoveredHouse.X - minbuyrange, 0); x <= hoveredHouse.X + minbuyrange && x < DataSize; x++)
					{
						ushort p = _data[x + y * DataSize];
						if (p > 0 && p < cheapestPrice)
						{
							cheapestPrice = p;
							cheapest = new Vector2i(x, y);
						}
					}
				}

				if (cheapest.X > 0 && cheapestPrice > 0 && !_sol.Contains(cheapest))
				{
					_sol.Add(cheapest);
					UpdateAll();
				}
			}

			const int minbuyrange2 = 500;

			if (Window.KeyboardState.IsKeyPressed(Keys.L))
			{
				ushort cheapestPrice = 65535;
				Vector2i cheapest = new Vector2i(-1);

				for (int y = Math.Max(hoveredHouse.Y - minbuyrange2, 0); y <= hoveredHouse.Y + minbuyrange2 && y < DataSize; y++)
				{
					for (int x = Math.Max(hoveredHouse.X - minbuyrange2, 0); x <= hoveredHouse.X + minbuyrange2 && x < DataSize; x++)
					{
						ushort p = _data[x + y * DataSize];
						if (p > 0 && p < cheapestPrice)
						{
							cheapestPrice = p;
							cheapest = new Vector2i(x, y);
						}
					}
				}

				if (cheapest.X > 0 && cheapestPrice > 0 && !_sol.Contains(cheapest))
				{
					_sol.Add(cheapest);
					UpdateAll();
				}
			}

			if (Window.KeyboardState.IsKeyPressed(Keys.D))
			{
				(_sol, _sol2) = (_sol2, _sol);
				UpdateAll();
			}

			// Render
			Device.BindPipeline(_psoDrawDataset);
			Device.BindGlobalUniforms(_psoDrawDataset.ShaderFragment);
			Device.BindTexture(_texColors, 0);
			Device.ShaderFragment.SetUniformI("pricedif", _visualizePriceDif ? 1 : 0);
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			Device.BindPipeline(_psoDrawHouses);
			Device.BindGlobalUniforms(_psoDrawHouses.ShaderVertex);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _bufferHouses);
			GL.PointSize(4f);
			GL.DrawArrays(PrimitiveType.Points, 0, _sol.Count);

			ImGuiRenderer.BeforeLayout((float)_currentDeltaT);

			// Draw our UI
			if (ImGui.Begin("Stuff"))
			{
				ImGui.TextUnformatted("Controls: scroll to zoom, hold spacebar and move mouse to pan");
				ImGui.TextUnformatted($"Press R to remove, B to buy, M to buy cheapest in range {minbuyrange}, or L for range {minbuyrange2}");
				ImGui.TextUnformatted($"Press D to swap to secondary sol");
				ImGui.TextUnformatted($"Hovered: {_hoveredPrice} House: {hoveredHouse}");
				ImGui.TextUnformatted($"Total price: {_solPrice} Count: {_sol.Count}");
				ImGui.TextUnformatted($"Covers all: {_valid}");

				List<Vector2i> tryLoadSol()
				{
					List<Vector2i> sol = new List<Vector2i>();
					try
					{
						int count = int.Parse(Console.ReadLine());
						for (int i = 0; i < count; i++)
						{
							int x, y;
							var line = Console.ReadLine().Split(' ');
							x = int.Parse(line[1]);
							y = int.Parse(line[0]);
							sol.Add(new Vector2i(x, y));
						}
					}
					catch (Exception e)
					{
						Console.WriteLine(e.ToString());
						return null;
					}
					return sol;
				}

				if(ImGui.Button("Paste sol into console"))
				{
					var sol = tryLoadSol();
					if(sol != null)
					{
						_sol = sol;
						UpdateAll();
					}
				}

				if (ImGui.Button("Paste secondary sol"))
				{
					var sol = tryLoadSol();
					if (sol != null)
					{
						_sol2 = sol;
						UpdateAll();
					}
				}

				if (ImGui.Button("Save sol (autonamed)"))
				{
					try
					{
						UpdateAll();

						if(!_valid)
						{
							throw new Exception("Solution not valid!");
						}

						using (StreamWriter sw = new StreamWriter($"editsol_{_solPrice}.out"))
						{
							sw.WriteLine(_sol.Count);
							foreach(var s in _sol)
							{
								sw.WriteLine($"{s.Y} {s.X}");
							}
						}

						Console.WriteLine("Saved successfully");
					}
					catch(Exception e)
					{
						Console.WriteLine(e.ToString());
					}
				}

				if (ImGui.Button("Refresh colors (press if you edit shaders)"))
				{
					UpdateAll();
				}
				
				if (ImGui.Button("Export image"))
				{
					ExportImage();
					UpdateAll();
				}

				ImGui.Checkbox("See price difference from clicked (green = more, blue = less)", ref _visualizePriceDif);

				ImGui.End();
			}

			// Call AfterLayout now to finish up and draw all the things
			ImGuiRenderer.AfterLayout();
		}

		static ushort[] LoadDataset(string file)
		{
			ushort[] data = new ushort[DataSize * DataSize];
			using (BinaryReader br = new BinaryReader(File.OpenRead(file)))
			{
				for (int i = 0; i < data.Length; i++)
				{
					data[i] = br.ReadUInt16();
				}
			}
			return data;
		}
	}
}
